import axiosClient from './instances/axiosClient';

const updateOrder = (_id, data) => {
  return axiosClient({
    method: 'PUT',
    data,
    url: `/orders/${_id}`,
  });
};

const createOrder = (data) => {
  return axiosClient({
    method: 'POST',
    data,
    url: '/orders',
  });
};

const createOrderItems = (data) => {
  return axiosClient({
    method: 'POST',
    data,
    url: '/order-items',
  });
};

const queryUserOrdersList = (userId) => {
  return axiosClient({
    method: 'GET',
    url: `/orders/user?user=${userId}`,
  });
};

const getOrderCount = () => {
  return axiosClient({
    method: 'GET',
    url: `/orders/count`,
  });
};

const queryOrdersList = (query) => {
  return axiosClient({
    method: 'GET',
    url: `/orders${query}`,
  });
};

const ORDER_API = {
  createOrder,
  createOrderItems,
  queryUserOrdersList,
  queryOrdersList,
  updateOrder,
  getOrderCount,
};

export default ORDER_API;
